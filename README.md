# CI for osbuild images project

This repository essentially is a mirror of https://github.com/osbuild/images

For each PR on github a _new branch_ `PR-*` is created here to run CI, see trigger here:
https://github.com/osbuild/images/blob/main/.github/workflows/trigger-gitlab.yml
